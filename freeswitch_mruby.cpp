#include "freeswitch_mruby.h"

Session::Session(char *uuid, CoreSession *a_leg):CoreSession(uuid, a_leg)
{
  
}

Session::~Session()
{
  destroy();
}

bool Session::begin_allow_threads()
{
  return false;
}

bool Session::end_allow_threads()
{
  return false;
}

void Session::check_hangup_hook()
{
}

switch_status_t Session::run_dtmf_callback(void *input, switch_input_type_t itype)
{
  return SWITCH_STATUS_FALSE;
}

void Session::destroy(void)
{
  CoreSession::destroy();
}
