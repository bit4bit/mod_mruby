FROM centos:7

RUN yum install -y https://files.freeswitch.org/repo/yum/centos-release/freeswitch-release-repo-0-1.noarch.rpm epel-release
RUN yum install -y freeswitch freeswitch-config-vanilla freeswitch-devel gcc-c++ make
RUN yum install -y wget ruby rake unzip

RUN cd /usr/src \
  && wget https://github.com/mruby/mruby/archive/2.1.2.zip -O mruby.zip \
  && unzip mruby.zip

COPY ./docker-files/event_socket.conf.xml /etc/freeswitch/autoload_configs/
COPY ./docker-files/mruby/build_config.rb /usr/src/mruby-2.1.2/
RUN cd /usr/src/mruby-2.1.2 \
  && rake \
  && cp -r build/FS/lib/*.a /usr/lib \
  && cp -r include /usr/
RUN mkdir /usr/src/mod_mruby

ENV FS_INCLUDES /usr/include
ENV FS_MODULES /usr/lib64/freeswitch/mod

EXPOSE 8021
EXPOSE 5060
EXPOSE 16384-32768

VOLUME ["/usr/src/mod_mruby"]
WORKDIR /usr/src/mod_mruby

CMD ["/bin/bash"]
