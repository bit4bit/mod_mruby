extern "C" {
#include <mruby.h>
#include <mruby/compile.h>
#include <mruby/class.h>
#include <mruby/data.h>
#include <mruby/variable.h>
}

#include "freeswitch_gem.h"
#include "freeswitch_mruby.h"


#define MRB_STRUCT_FOR(for_name, for_class) \
  struct mrb_ ##for_name  {  for_class *obj; };\
  \
  static void mrb_ ## for_name ## _free(mrb_state *mrb, void *p)\
  {\
  struct mrb_##for_name *local = (struct mrb_##for_name *)p; \
  delete local->obj; \
  mrb_free(mrb, local);\
  }\
  \
  static const struct mrb_data_type mrb_##for_name##_type = { #for_class, mrb_##for_name##_free };

#define MRB_MAKE_STRUCT_FOR(mrb, for_name) \
  struct mrb_ ##for_name * for_name = (struct mrb_##for_name *) mrb_malloc(mrb, sizeof(struct mrb_##for_name)); \
  if (!for_name) {\
  mrb_raise(mrb, E_RUNTIME_ERROR, "cant' allocate mrb_" #for_name "\n"); \
  }

#define MRB_DATA_GET_PTR_FOR(mrb, instance, for_name) \
  struct mrb_ ##for_name * for_name = DATA_GET_PTR(mrb, instance, &mrb_##for_name##_type, struct mrb_##for_name);\
  if (!for_name) {\
  mrb_raise(mrb, E_ARGUMENT_ERROR, "unitialize " # for_name);\
  }\
  switch_assert(for_name->obj);


static struct RClass *get_class_under_module(mrb_state *mrb, const char *mod_name, const char *class_name)
{
  struct RClass *module = mrb_module_get(mrb, mod_name);
  return mrb_class_get_under(mrb, module, class_name);
}

//====API
MRB_STRUCT_FOR(api, API)

/*static mrb_value mrb_api_wrap(mrb_state *mrb, struct RClass *klass, struct mrb_api *api)
{
  return mrb_obj_value(Data_Wrap_Struct(mrb, klass, &mrb_api_type, api));
  }*/

static mrb_value api_initialize_method(mrb_state *mrb, mrb_value self)
{
  MRB_MAKE_STRUCT_FOR(mrb, api);
  api->obj = new API();

  mrb_data_init(self, api, &mrb_api_type);
  return self;
}


mrb_value api_getTime_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, api);

  return mrb_str_new_cstr(mrb, api->obj->getTime());
}

mrb_value api_execute_method(mrb_state *mrb, mrb_value self)
{
  char *command;
  char *args = NULL;
  mrb_value api_result;

  MRB_DATA_GET_PTR_FOR(mrb, self, api);
  mrb_get_args(mrb, "z|z", &command, &args);

  if (!strcasecmp(command, "mrubyrun")) {
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Posible recursive API call is not allowed\n");
    return mrb_false_value();
  }

  const char *result = api->obj->execute(command, switch_str_nil(args));
  api_result = mrb_str_new_cstr(mrb, result);
  
  return api_result;
}


mrb_value api_executeString_method(mrb_state *mrb, mrb_value self)
{
  char *command;
  mrb_value api_result;

  MRB_DATA_GET_PTR_FOR(mrb, self, api);
  mrb_get_args(mrb, "z", &command);

  if (!strcasecmp(command, "mrubyrun")) {
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Posible recursive API call is not allowed\n");
    return mrb_false_value();
  }

  const char *result = api->obj->executeString(command);
  api_result = mrb_str_new_cstr(mrb, result);
  
  return api_result;
}



//==Event
MRB_STRUCT_FOR(event, Event)

static mrb_value event_initialize_method(mrb_state *mrb, mrb_value self)
{

  MRB_MAKE_STRUCT_FOR(mrb, event);

  char *type;
  char *subclass_name;
  mrb_get_args(mrb, "z|z", &type, &subclass_name);
  event->obj = new Event(type, subclass_name);

  mrb_data_init(self, event, &mrb_event_type);
  return self;
}

static mrb_value event_addHeader_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, event);

  char *header_name;
  char *value;
  mrb_get_args(mrb, "zz", &header_name, &value);
  bool event_result = event->obj->addHeader(header_name, value);
  
  if (event_result)
    return mrb_true_value();
  return mrb_false_value();
}

static mrb_value event_getHeader_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, event);

  char *header_name;
  mrb_get_args(mrb, "z", &header_name);
  return mrb_str_new_cstr(mrb, event->obj->getHeader(header_name));
}


static mrb_value event_addBody_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, event);

  char *body;
  mrb_get_args(mrb, "z", &body);

  if (event->obj->addBody(body))
    return mrb_true_value();
  return mrb_false_value();
}

static mrb_value event_fire_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, event);
  
  if (event->obj->fire())
    return mrb_true_value();
  return mrb_false_value();
}

//==EVENT_CONSUMER
MRB_STRUCT_FOR(event_consumer, EventConsumer)

static mrb_value event_consumer_initialize_method(mrb_state *mrb, mrb_value self)
{
  MRB_MAKE_STRUCT_FOR(mrb, event_consumer);

  char *event_name;
  char *subclass_name;
  int len = 5000;
  mrb_get_args(mrb, "|zzi", &event_name, &subclass_name, &len);

  event_consumer->obj = new EventConsumer(event_name, switch_str_nil(subclass_name), len);
  mrb_data_init(self, event_consumer, &mrb_event_consumer_type);
  return self;
}

static mrb_value event_consumer_bind_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, event_consumer);

  char *event_name;
  char *subclass_name;
  mrb_get_args(mrb, "z|z", &event_name, &subclass_name);

  int result = event_consumer->obj->bind(event_name, switch_str_nil(subclass_name));
  return mrb_fixnum_value(result);
}

static mrb_value event_consumer_cleanup_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, event_consumer);
  event_consumer->obj->cleanup();
  return mrb_true_value();
}


static mrb_value event_consumer_pop_method(mrb_state *mrb, mrb_value self)
{
  bool block = false;
  int timeout = 0;
  mrb_get_args(mrb, "|bi", &block, &timeout);

  MRB_DATA_GET_PTR_FOR(mrb, self, event_consumer);
  
  Event *popped = event_consumer->obj->pop(block, timeout);

  if (popped == NULL) {
    return mrb_nil_value();
  }

  //crea instancia sin inicializar y asignar evento
  struct RClass *fs_module = mrb_module_get(mrb, "Freeswitch");
  struct RClass *class_event = mrb_class_get_under(mrb, fs_module, "Event");
  mrb_value new_event = mrb_obj_value((struct RObject*)mrb_obj_alloc(mrb, MRB_TT_DATA, class_event));
  struct mrb_event *tmp = (struct mrb_event *) DATA_PTR(new_event);
  if (tmp) {
    mrb_free(mrb, tmp);
  }
  mrb_data_init(new_event, NULL, &mrb_event_type);

  struct mrb_event *nevent = (struct mrb_event *) mrb_malloc(mrb, sizeof(struct mrb_event));
  if (!nevent) {
    mrb_raise(mrb, E_RUNTIME_ERROR, "can't allocate mrb_event");
  }
  nevent->obj = popped;
  mrb_data_init(new_event, nevent, &mrb_event_type);
  return new_event;
}

//=CoreSession
MRB_STRUCT_FOR(session, Session)

static mrb_value session_initialize_method(mrb_state *mrb, mrb_value self)
{
  MRB_MAKE_STRUCT_FOR(mrb, session);

  char *uuid;
  mrb_value a_leg;
  mrb_get_args(mrb, "z|o", &uuid, &a_leg);

  struct RClass *class_session = get_class_under_module(mrb, "Freeswitch", "Session");
  if (mrb_obj_is_instance_of(mrb, a_leg, class_session)) {
    struct mrb_session *a_session = DATA_GET_PTR(mrb, a_leg, &mrb_session_type, struct mrb_session);
    if (!a_session) {
      mrb_raise(mrb, E_ARGUMENT_ERROR, "a_leg session unitialize");
    }
    
    session->obj = new Session(uuid, a_session->obj);
  } else {
    session->obj = new Session(uuid);
  }
  mrb_data_init(self, session, &mrb_session_type);
  return self;
}

static mrb_value session_print_method(mrb_state *mrb, mrb_value self)
{
  char *txt;
  mrb_get_args(mrb, "z", &txt);

  MRB_DATA_GET_PTR_FOR(mrb, self, session);
  return mrb_fixnum_value(session->obj->print(txt));
}

static mrb_value session_answer_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);
  return mrb_fixnum_value(session->obj->answer());
}

static mrb_value session_preAanswer_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);
  return mrb_fixnum_value(session->obj->preAnswer());
}

static mrb_value session_hangup_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *cause = {0};
  mrb_get_args(mrb, "|z", &cause);
  if (zstr(cause)) {
    session->obj->hangup();
  } else {
    session->obj->hangup(cause);
  }
  return mrb_nil_value();
}

static mrb_value session_ready_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  if (session->obj->ready())
    return mrb_true_value();
  
  return mrb_false_value();
}

static mrb_value session_answered_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  if (session->obj->answered())
    return mrb_true_value();
  
  return mrb_false_value();
}

static mrb_value session_sleep_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  int ms;
  int sync = 0;
  mrb_get_args(mrb, "i|i", &ms, &sync);
  return mrb_fixnum_value(session->obj->sleep(ms, sync));
}

static mrb_value session_execute_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *app;
  char *data = NULL;
  mrb_get_args(mrb, "z|z", &app, &data);

  session->obj->execute(app, data);
  return mrb_nil_value();
}

static mrb_value session_speak_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *text;
  mrb_get_args(mrb, "z|z", &text);

  return mrb_fixnum_value(session->obj->speak(text));
}

static mrb_value session_bridged_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  if (session->obj->bridged())
    return mrb_true_value();
  
  return mrb_false_value();
}

static mrb_value session_mediaReady_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  if (session->obj->mediaReady())
    return mrb_true_value();
  
  return mrb_false_value();
}

static mrb_value session_setVariable_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *var;
  char *val;
  mrb_get_args(mrb, "zz", &var, &val);
  session->obj->setVariable(var, val);
  
  return mrb_nil_value();
}

static mrb_value session_getVariable_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *var;
  mrb_get_args(mrb, "z", &var);
  return mrb_str_new_cstr(mrb, session->obj->getVariable(var));
}

static mrb_value session_hangupCause_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  return mrb_str_new_cstr(mrb, session->obj->hangupCause());
}

static mrb_value session_get_uuid_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  return mrb_str_new_cstr(mrb, session->obj->get_uuid());
}

static mrb_value session_getState_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  return mrb_str_new_cstr(mrb, session->obj->getState());
}

static mrb_value session_setAutoHangup_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  mrb_bool val;
  mrb_get_args(mrb, "b", &val);
  return mrb_fixnum_value(session->obj->setAutoHangup((bool)val));
}

static mrb_value session_recordFile_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *file_name;
  int time_limit = 0;
  int silence_threshold = 0;
  int silence_hits = 0;
  mrb_get_args(mrb, "z|iii", &file_name, &time_limit, &silence_threshold, &silence_hits);
  return mrb_fixnum_value(session->obj->recordFile(file_name, time_limit, silence_threshold, silence_hits));
}

static mrb_value session_setPrivate_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *key;
  void *obj = mrb_malloc(mrb, sizeof(mrb_value));
  mrb_get_args(mrb, "zo", &key, obj);
  session->obj->setPrivate(key, obj);
  return mrb_nil_value();
}

static mrb_value session_getPrivate_method(mrb_state *mrb, mrb_value self)
{
  MRB_DATA_GET_PTR_FOR(mrb, self, session);

  char *key;
  mrb_get_args(mrb, "z", &key);
  mrb_value *  pobj = (mrb_value *) session->obj->getPrivate(key);

  if (!pobj)
    return mrb_nil_value();
    
  return *pobj;
}

mrb_value freeswitch_consoleLog_method(mrb_state *mrb, mrb_value self)
{
  char *level;
  char *msg;
  mrb_get_args(mrb, "zz", &level, &msg);

  consoleLog(level, msg);
  
  return mrb_nil_value();
}

static mrb_value freeswitch_bridge_method(mrb_state *mrb, mrb_value self)
{
  mrb_value obj_session_a;
  mrb_value obj_session_b;
  mrb_get_args(mrb, "oo", &obj_session_a, &obj_session_b);

  struct mrb_session *session_a = DATA_GET_PTR(mrb, obj_session_a, &mrb_session_type, struct mrb_session);
  if (!session_a) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "session_a unitialize");
  }

  struct mrb_session *session_b = DATA_GET_PTR(mrb, obj_session_b, &mrb_session_type, struct mrb_session);
  if (!session_b) {
    mrb_raise(mrb, E_ARGUMENT_ERROR, "session_b unitialize");
  }

  bridge(*(session_a->obj), *(session_b->obj));
  return mrb_nil_value();
}

void mrb_freeswitch_gem_init(mrb_state *mrb)
{
  struct RClass *fs_module;
  struct RClass *class_api;
  struct RClass *class_event;
  struct RClass *class_event_consumer;
  struct RClass *class_session;
  
  fs_module = mrb_define_module(mrb, "Freeswitch");

  mrb_define_module_function(mrb, fs_module, "consoleLog", freeswitch_consoleLog_method, MRB_ARGS_REQ(2));
  mrb_define_module_function(mrb, fs_module, "bridge", freeswitch_bridge_method, MRB_ARGS_REQ(2));

  class_api = mrb_define_class_under(mrb,fs_module, "API", mrb->object_class);
  MRB_SET_INSTANCE_TT(class_api, MRB_TT_DATA);
  mrb_define_method(mrb, class_api, "initialize", api_initialize_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_api, "execute", api_execute_method, MRB_ARGS_ARG(1,1));
  mrb_define_method(mrb, class_api, "executeString", api_executeString_method, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, class_api, "getTime", api_getTime_method, MRB_ARGS_NONE());

  
  class_event = mrb_define_class_under(mrb, fs_module, "Event", mrb->object_class);
  MRB_SET_INSTANCE_TT(class_event, MRB_TT_DATA);
  mrb_define_method(mrb, class_event, "initialize", event_initialize_method, MRB_ARGS_ARG(1, 1));
  mrb_define_method(mrb, class_event, "addHeader", event_addHeader_method, MRB_ARGS_REQ(2));
  mrb_define_method(mrb, class_event, "getHeader", event_getHeader_method, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, class_event, "addBody", event_addBody_method, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, class_event, "fire", event_fire_method, MRB_ARGS_NONE());

  class_event_consumer = mrb_define_class_under(mrb, fs_module, "EventConsumer", mrb->object_class);
  MRB_SET_INSTANCE_TT(class_event_consumer, MRB_TT_DATA);
  mrb_define_method(mrb, class_event_consumer, "initialize", event_consumer_initialize_method, MRB_ARGS_OPT(3));
  mrb_define_method(mrb, class_event_consumer, "pop", event_consumer_pop_method, MRB_ARGS_OPT(2));
  mrb_define_method(mrb, class_event_consumer, "bind", event_consumer_bind_method, MRB_ARGS_ARG(1, 1));
  mrb_define_method(mrb, class_event_consumer, "cleanup", event_consumer_cleanup_method, MRB_ARGS_NONE());

  class_session = mrb_define_class_under(mrb, fs_module, "Session", mrb->object_class);
  MRB_SET_INSTANCE_TT(class_session, MRB_TT_DATA);
  mrb_define_method(mrb, class_session, "initialize", session_initialize_method, MRB_ARGS_ARG(1,1));
  mrb_define_method(mrb, class_session, "print", session_print_method, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, class_session, "preAnswer", session_preAanswer_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "answer", session_answer_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "hangup", session_hangup_method, MRB_ARGS_OPT(1));
  mrb_define_method(mrb, class_session, "ready", session_ready_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "answered", session_answered_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "bridged", session_bridged_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "mediaReady", session_mediaReady_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "sleep", session_sleep_method, MRB_ARGS_ARG(1,1));
  mrb_define_method(mrb, class_session, "execute", session_execute_method, MRB_ARGS_ARG(1, 1));
  mrb_define_method(mrb, class_session, "speak", session_speak_method, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, class_session, "setVariable", session_setVariable_method, MRB_ARGS_REQ(2));
  mrb_define_method(mrb, class_session, "getVariable", session_getVariable_method, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, class_session, "setAutoHangup", session_setAutoHangup_method, MRB_ARGS_REQ(1));
  mrb_define_method(mrb, class_session, "hangupCause", session_hangupCause_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "getState", session_getState_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "recordFile", session_recordFile_method, MRB_ARGS_ARG(1, 3));
  mrb_define_method(mrb, class_session, "get_uuid", session_get_uuid_method, MRB_ARGS_NONE());
  mrb_define_method(mrb, class_session, "setPrivate", session_setPrivate_method, MRB_ARGS_REQ(2));
  mrb_define_method(mrb, class_session, "getPrivate", session_getPrivate_method, MRB_ARGS_REQ(2));
}
