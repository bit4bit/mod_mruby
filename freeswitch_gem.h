#ifndef MRUBY_FREESWITCH_H
#define MRUBY_FREESWITCH_H

#include <mruby.h>
#include <switch_cpp.h>

void mrb_freeswitch_gem_init(mrb_state *mrb);

#endif
