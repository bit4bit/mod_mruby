def run()
  session_a = Freeswitch::Session.new("{ignore_early_media=true,hangup_after_bridge=true}user/1000")

  if session_a.ready()
    session_a.setVariable("ringback", "%(2000,4000,440,480)")
    session_b = Freeswitch::Session.new("{hangup_after_bridge=true}user/1007")
    if session_b.ready()
      Freeswitch.consoleLog("INFO", "briding 1000 1007\n")
      Freeswitch.bridge(session_a, session_b)
      Freeswitch.consoleLog("INFO", "after bridge\n")
      session_b.hangup()
    end
  end
  session_a.hangup()
end

def main(val)
  begin
    run()
  rescue Exception => e
    Freeswitch.consoleLog("ERR", e.message) 
    Freeswitch.consoleLog("ERR", e.backtrace.inspect)
  end
end
