MODNAME = mod_mruby.so
MODOBJ = mod_mruby.o freeswitch_gem.o freeswitch_mruby.o
MODCFLAGS = -Wall -Werror
MODLDFLAGS = /usr/lib/libmruby.a /usr/lib/libmruby_core.a

FS_INCLUDES ?= /usr/include/freeswitch/
FS_MODULES ?= /usr/lib/freeswitch/mod
MRUBY_INCLUDES ?= /usr/include

CC = g++
CFLAGS = -fPIC -g -ggdb -I$(FS_INCLUDES) -I$(MRUBY_INCLUDES) $(MODCFLAGS)
LDFLAGS = $(MODLDFLAGS)

.PHONY: all
all: $(MODNAME)

$(MODNAME): $(MODOBJ)
	$(CC) -shared -o $@ $(MODOBJ) $(LDFLAGS)

.cpp.o: $<
	$(CC) $(CFLAGS) -o $@ -c $<

.PHONY: clean
clean:
	rm -f $(MODNAME) $(MODOBJ)

.PHONY: install
install: $(MODNAME)
	install $(MODNAME) $(FS_MODULES)
