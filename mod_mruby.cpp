#include <switch.h>

#include <mruby.h>
#include <mruby/compile.h>
#include "freeswitch_gem.h"


SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_mruby_shutdown);
SWITCH_MODULE_RUNTIME_FUNCTION(mod_mruby_runtime);
SWITCH_MODULE_LOAD_FUNCTION(mod_mruby_load);

SWITCH_MODULE_DEFINITION(mod_mruby, mod_mruby_load, mod_mruby_shutdown, NULL);

static switch_status_t mruby_run(const char *filename, const char *arg)
{
  mrb_state *mrb = mrb_open();
  switch_assert(mrb);
  mrb_freeswitch_gem_init(mrb);

  FILE *fp = fopen(filename, "r");
  if (!fp) {
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Couldn't open file %s\n", filename);
    mrb_close(mrb);
    return SWITCH_STATUS_FALSE;
  }
  
  mrb_value obj = mrb_load_file(mrb, fp);

  //TODO main a configuracion de modulo
  mrb_funcall(mrb, obj, "main", 1, mrb_str_new_cstr(mrb, arg));
  if (mrb->exc) {
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Get exception\n");
  }
  mrb_close(mrb);
  return SWITCH_STATUS_SUCCESS;
}

SWITCH_STANDARD_API(mrubyrun_api_function)
{
  mruby_run(cmd, "");
  return SWITCH_STATUS_SUCCESS;
}

SWITCH_STANDARD_APP(mruby_app_function)
{
  if (zstr(data)) {
    switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "file required\n");
  }
  
  char *uuid = switch_core_session_get_uuid(session);
  mruby_run(data, uuid);
}

SWITCH_MODULE_LOAD_FUNCTION(mod_mruby_load)
{
  switch_api_interface_t *api_interface;
  switch_application_interface_t *app_interface;
  
  *module_interface = switch_loadable_module_create_module_interface(pool, modname);

  SWITCH_ADD_API(api_interface, "mrubyrun", "run a script", mrubyrun_api_function, "<filename>");
  SWITCH_ADD_APP(app_interface, "mruby", "Launch mruby", "Run mruby in channel", mruby_app_function, "<filename>", SAF_SUPPORT_NOMEDIA);
  return SWITCH_STATUS_SUCCESS;
}

SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_mruby_shutdown)
{
  return SWITCH_STATUS_SUCCESS;
}
