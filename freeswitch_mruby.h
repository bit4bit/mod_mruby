#ifndef FREESWITCH_MRUBY_H
#define FREESWITCH_MRUBY_H

extern "C" {
  #include <switch.h>
}

#include <switch_cpp.h>

class Session:public CoreSession {
public:
  Session(char *uuid, CoreSession *a_leg =NULL);
  ~Session();
  
  virtual void destroy(void);
  virtual bool begin_allow_threads();
  virtual bool end_allow_threads();
  virtual void check_hangup_hook();
  virtual switch_status_t run_dtmf_callback(void *input, switch_input_type_t itype);
};

#endif
